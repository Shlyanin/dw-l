<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\BlogController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [BlogController::class, 'index'])->name('homePage');

Route::get('/category/{id}', [BlogController::class, 'getPostsByCategory'])->name('getPostsByCategory');

Route::get('/category/{category_id}/{post_id}', [BlogController::class, 'getPost'])->name('getPost');

Route::get('add/', [BlogController::class, 'add'])->middleware('auth');

Route::post('add/', [BlogController::class, 'create']);

Route::get('edit/{post_id}', [BlogController::class, 'edit'])->name('editPost')->middleware('auth');

Route::post('edit/{post_id}', [BlogController::class, 'update']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::middleware(['role:admin'])->prefix('admin_panel')->group(function () {
    Route::get('/', [\App\Http\Controllers\Admin\HomeController::class, 'index'])->name('homeAdmin');
    Route::resource('category', \App\Http\Controllers\Admin\CategoryController::class);
});


