<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'text' => 'required',
            // 'slug' => 'required',
            'title' => 'required|min:10',
            'price' => 'required',
            'category_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Укажите плиз :attribute пожалуйста',
            'title.min' => 'Название должно быть не менее :min символов',
            // 'slug.min' => 'Символьный код должен быть не менее :min символов',
        ];
    }

}
