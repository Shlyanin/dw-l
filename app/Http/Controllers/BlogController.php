<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostRequest;
use App\Models\Category;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BlogController extends Controller
{
    public function index()
    {
        $posts = Post::paginate(4);
        $categories = Category::withCount('posts')
            ->orderBy('posts_count', 'desc')
            ->orderBy('title')
            ->get();

        return view('pages.blog', [
            'posts' => $posts,
            'categories' => $categories,
            'title' => 'Все объявления',
            'user_id' => Auth::user() ? Auth::user()->id : 0,
            'current_category' => null
        ]);
    }

    public function getPostsByCategory($id)
    {
        $current_category = Category::where('id', $id)->first();
        $categories = Category::withCount('posts')
            ->orderBy('posts_count', 'desc')
            ->orderBy('title')
            ->get();

        return view('pages.blog', [
            'posts' => $current_category->posts()->paginate(4),
            'categories' => $categories,
            'current_category' => $current_category,
            'title' => $current_category->title,
            'user_id' => Auth::user() ? Auth::user()->id : 0
        ]);
    }

    public function getPost($category_id, $post_id)
    {
        $post = Post::where('id', $post_id)->first();
        $current_category = Category::where('id', $category_id)->first();
        $categories = Category::withCount('posts')
            ->orderBy('posts_count', 'desc')
            ->orderBy('title')
            ->get();
        $user = User::where('id', $post->user_id)->first();

        return view('pages.blog-detail', [
            'post' => $post,
            'posts' => $current_category->posts()->paginate(4),
            'categories' => $categories,
            'current_category' => $current_category,
            'user' => $user
        ]);
    }

    public function add()
    {
        $categories = Category::orderBy('title')->get();

        return view('pages.blog-add', [
            'title' => 'Разместить объявление',
            'categories' => $categories,
        ]);
    }

    public function create(PostRequest $request)
    {
        $current_category = Category::where('id', $request->category_id)->first();

        $post = new Post();
        $post->title = $request->title;
        $post->price = $request->price;
        $post->description = $request->description;
        $post->text = $request->text;
        $post->category_id = $request->category_id;
        $post->user_id = Auth::user()->id;
        $add = $post->save();

        /*$add = DB::table('posts')->insert([
            'title' => $request->title,
            'price' => $request->price,
            'description' => $request->description,
            'text' => $request->text,
            'category_id' => $request->category_id,
            'user_id' => Auth::user()->id
        ]);*/

        if ($add) {
            return redirect(route('getPostsByCategory', $current_category->id));
        }

    }

    public function edit($post_id)
    {
        $post = Post::where('id', $post_id)->first();
        $categories = Category::orderBy('title')->get();

        if (Auth::user()->id != $post->user_id) {
            return redirect(route('homePage'));
        }

        return view('pages.blog-edit', [
            'title' => $post->title,
            'post' => $post,
            'categories' => $categories,
        ]);
    }

    public function update(PostRequest $request)
    {
        $post = Post::where('id', $request->id)->first();
        $current_category = Category::where('id', $request->category_id)->first();

        $post->title = $request->title;
        $post->category_id = $request->category_id;
        $post->description = $request->description;
        $post->text = $request->text;

        $post->save();

        return redirect(route('getPost', [$current_category->id, $request->id]));
    }
}
