<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'title' => 'Автомобили',
            'slug' => 'auto',
        ]);

        DB::table('categories')->insert([
            'title' => 'Недвижимость',
            'slug' => 'retail',
        ]);

        DB::table('categories')->insert([
            'title' => 'Прочее',
            'slug' => 'other',
        ]);
    }
}
