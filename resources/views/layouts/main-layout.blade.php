<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
</head>
<body>

@include('includes.nav')

<div class="container">
    <h1 class="mt-5 mb-4">
        <div class="row">
            <div class="col-6">
                @yield('title')
            </div>
            <div class="col-6" style="text-align: right;">
                @unless (!\Illuminate\Support\Facades\Auth::check())
                    <a href="/add/">
                        <div class="btn btn-success">Разместить объявление +</div>
                    </a>
                @endunless
            </div>
        </div>
    </h1>
    @yield('content')
</div>

<script src="/js/bootstrap.min.js"></script>
</body>
</html>
