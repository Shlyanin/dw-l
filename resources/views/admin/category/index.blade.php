@extends('layouts.admin-layout')

@section('title', 'Список категорий')

@section('content')
    <section class="content">
        <div class="container-fluid">

            <div class="row mb-4">
                <div class="col-12">
                    <a href="{{ route('category.create') }}" class="btn btn-info">Добавить</a>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <table id="example2" class="table table-bordered table-hover dataTable dtr-inline" role="grid"
                           aria-describedby="example2_info">
                        <thead>
                        <tr role="row">
                            <th class="sorting sorting_asc" tabindex="0" aria-controls="example2" rowspan="1"
                                colspan="1"
                                aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                Название
                            </th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($categories as $category)
                            <tr class="odd">
                                <td class="dtr-control sorting_1" tabindex="0">{{$category->title}}</td>
                            </tr>
                        @endforeach

                    </table>
                </div>
            </div>
        </div>
        </div>
    </section>
@endsection
