<nav aria-label="breadcrumb">

    <div class="mb-4">
        <a href="/" class="btn btn-success w-100">Все объявления</a>
    </div>
    @foreach($categories as $category)
        @if($current_category != null)
            @if($current_category->id == $category->id)
                <div class="mb-4">
                    <a href="{{route('getPostsByCategory', $category->id)}}"
                       class="btn btn-outline-secondary active w-100">{{$category->title}}

                        <span class="badge badge-secondary " style="background-color: #6c757d;">
                    {{$category->posts_count}} шт.
                </span>
                    </a>
                </div>
            @else
                <div class="mb-4">
                    <a href="{{route('getPostsByCategory', $category->id)}}"
                       class="btn btn-outline-secondary w-100">{{$category->title}}

                        <span class="badge badge-secondary" style="background-color: #6c757d;">
                    {{$category->posts_count}} шт.
                </span>
                    </a>
                </div>
            @endif
        @else
            <div class="mb-4">
                <a href="{{route('getPostsByCategory', $category->id)}}"
                   class="btn btn-outline-secondary w-100">{{$category->title}}

                    <span class="badge badge-secondary" style="background-color: #6c757d;">
                    {{$category->posts_count}} шт.
                </span>
                </a>
            </div>
        @endif
    @endforeach
</nav>

