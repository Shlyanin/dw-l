@extends('layouts.main-layout')

@section('title', $title)

@section('content')
    <div class="row">
        <div class="col-12">

            <form method="post">
                @csrf

                @if ($errors->any())
                    @foreach($errors->all() as $error)
                        <div class="alert alert-danger">{{$error}}</div>
                    @endforeach
                @endif

                <div class="form-group mb-4">
                    <label for="title">Товар или услуга</label>
                    <input type="text" class="form-control" id="title"
                           value="{{old('title')}}"
                           name="title"
                           placeholder="Например продам гараж...">
                </div>
                <div class="form-group mb-4">
                    <label for="price">Цена</label>
                    <input type="text" class="form-control" id="price"
                           name="price"
                           value="{{old('price')}}"
                           placeholder="">
                </div>
                <div class="form-group mb-4">
                    <label for="category_id">Категория</label>
                    <select class="form-control" id="category_id" name="category_id">
                        <option value=""></option>
                        @foreach($categories as $category)
                            <option value="{{$category->id}}">{{$category->title}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group mb-4">
                    <label for="description">Краткое опсание</label>
                    <textarea class="form-control" id="description" name="description" rows="2">{{old('description')}}</textarea>
                </div>
                <div class="form-group mb-4">
                    <label for="text">Подробное описание</label>
                    <textarea class="form-control" id="text" name="text" rows="5">{{old('text')}}</textarea>
                </div>
                <button type="submit" class="btn btn-primary">Добавить</button>
            </form>
        </div>
    </div>
@endsection
