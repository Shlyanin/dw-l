@extends('layouts.main-layout')

@section('title', $current_category->title . ' / ' . $post->title)

@section('content')
    <div class="row">
        <div class="col-2">
            @include('includes.categories-index')
        </div>
        <div class="col-10">
            <div>
                <a href="{{route('getPostsByCategory', $current_category->id)}}"
                   class="btn btn-outline-primary mb-4">Назад
                </a>
            </div>
            <div class="price mb-4">
                <b>Цена</b>: от {{$post->price}} руб.<br>
                <b>Имя</b>: {{$user->name}}<br>
                <b>E-mail</b>: {{$user->email}}<br>
                <b>Телефон</b>: +7 (923) 469-22-75<br>
            </div>
            <article>
                {!! $post->text !!}
            </article>
            <div class="image-block mb-4" style="display: flex; justify-content: space-between;">
                <img src="https://via.placeholder.com/200" alt="">
                <img src="https://via.placeholder.com/200" alt="">
                <img src="https://via.placeholder.com/200" alt="">
                <img src="https://via.placeholder.com/200" alt="">
                <img src="https://via.placeholder.com/200" alt="">
            </div>
            <div class="h4">Похожие объявления</div>
            <div class="flex" style="display: flex; align-items: start;">
                @foreach($posts as $post)
                    <div class="card" style="width: 18rem; margin-right: 20px;">
                        <img class="card-img-top" src="https://via.placeholder.com/500"
                             alt="{{$post->title}}">
                        <div class="card-body">
                            <h5 class="card-title">{{$post->title}}</h5>
                            <p class="card-text">{{$post->description}}</p>
                            <p class="card-text">От {{$post->price}} руб.</p>
                            <a href="{{route('getPost', [$post->category['id'], $post->id])}}"
                               class="btn btn-primary">Подробнее</a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
@endsection
