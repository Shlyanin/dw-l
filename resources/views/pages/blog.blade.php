@extends('layouts.main-layout')

@section('title', $title)

@section('content')
    <div class="row">
        <div class="col-2">
            @include('includes.categories-index')
        </div>
        <div class="col-10">
            @foreach($posts as $post)

                <div class="card mb-4">
                    <div class="card-header">
                        <a href="{{route('getPostsByCategory', $post->category['id'])}}">
                            {{$post->category['title']}}
                        </a>
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">{{$post->title}}</h5>
                        <p class="card-text">{{$post->description}}</p>
                        <p class="card-text">{{$post->price}} руб.</p>
                        <a href="{{route('getPost', [$post->category['id'], $post->id])}}"
                           class="btn btn-primary">
                            Подробнее
                        </a>
                        @unless (!\Illuminate\Support\Facades\Auth::check())
                            @if($user_id == $post->user_id)
                                <a href="{{route('editPost', [$post->id])}}"
                                   class="btn btn-primary">
                                    Редактировать
                                </a>
                            @endif
                        @endunless
                    </div>
                </div>
            @endforeach

            {{$posts->links('vendor.pagination.bootstrap-4')}}
        </div>
    </div>
@endsection
